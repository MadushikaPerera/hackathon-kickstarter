const SignUpModel = function() {
	
	function saveSignUp(fname, mongoose, cb) {
		//create model from mongoose object
		let SignUp = mongoose.model('SignUp', { 'fname': String });

		//create new instance from model
		firstName = new SignUp({ fname: fname });

		//call save on model instance and call the callack supplied from utils/processor.js to
		// return either total back to processor.js or the error
		// goto processor file functions callback
		firstName.save(function(err) {
			if(err)	cb(null, err);

			else {
				cb(fname, null);
			}
		});
	}

	return {
		saveSignUp: saveSignUp
	};
};

module.exports = SignUpModel;